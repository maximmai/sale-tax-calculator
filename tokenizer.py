"""
Tokenizer is a class that parses a text entry.
String position based instead of Regex.

"""
class Tokenizer:

	def __init__(self, string, config):
		tax_rate = config["tax_rate"]
		import_duty = config["import_duty"]
		exemption = config["books"] + config["foods"] + config["meds"]

		self.valid = False

		tokens = string.split(' ')

		try:	
			self.quantity = int(tokens[0]) # cast the first token to int
			self.is_imported = True if tokens[1] == "imported" else False
			self.name = tokens[1:-2]
			self.price = float(tokens[-1]) # cast the price to float
			self.tax = self.price * (1+tax_rate) if self.name not in exemption else 0.0
			self.duty = self.price * (1+import_duty) if self.is_imported else 0.0
			if self.quantity and self.name and self.price:
				self.valid = True
			else:
				self.valid = False
		except Exception, e:
			self.valid = False

	def get_total(self):
		if self.valid:
			return self.price + self.duty + self.tax
		else:
			return -1

	# override the toString method
	def __str__(self):
		if self.valid:
			return str(self.quantity) + " " + " ".join(self.name) + ": " + "{0:.2f}".format(self.get_total())
		else:
			return ""
