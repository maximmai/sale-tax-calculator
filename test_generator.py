# credit: http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
import random, string

# generate a price list of {"name": string, "price": float} tupples
# len(itemname) = 10
# len(price_list) = 100
price_list = [{"name" : ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10)), "price": random.uniform(1.00, 100.00)} for i in range(100)]

units = [''.join(random.choice(string.ascii_uppercase) for _ in range(3)) for i in range(5)]
# pick the first 10 items to be duty free

data = {
	"tax_rate" : 0.10,
	"import_duty" : 0.05,
	"foods" : [x["name"] for x in price_list[:5]],
	"books" : [x["name"] for x in price_list[5:10]],
	"meds" : [x["name"] for x in price_list[10:15]],
}


# list to hold customers
customers = []

# 10000 customers
for i in range(10000):
	customer_shopping_list = []

	# randomize a number to be the total number of items in this shopping list
	for j in range(random.choice(range(20))):

		# randomize an item from the price_list
		item = random.choice(price_list)

		# compose the line item
		if random.choice([True, False]):
			if random.choice([True, False]):
				line = "1 " + item["name"] + " at " + str(item["price"])
			else:
				line = "1 " + random.choice(units) + " of " + item["name"] + " at " + str(item["price"])
		else:
			if random.choice([True, False]):
				line = "1 imported " + item["name"] + " at " + str(item["price"])
			else:
				line = "1 imported " + random.choice(units) + " of " + item["name"] + " at " + str(item["price"])

		customer_shopping_list.append(line)

	customers.append(customer_shopping_list)


from tokenizer import Tokenizer
import json

with open('config.json', 'r') as f:
    config = data
    


"""
FUNCTIONAL TEST
"""
count = 1
for customer in customers:
	print "Output",count
	total = 0.0
	tax = 0.0
	for item in customer:
		t = Tokenizer(item, config)
		if t.valid:
			total = total + t.get_total()
			tax = tax + t.tax
			print t
	print "Sales Tax:", "{0:.2f}".format(tax)
	print "Total:", "{0:.2f}".format(total)
	print ""
	count = count + 1


"""
DIAGNOSTIC TEST
"""
# Set 1: Invalid Quantity
# Set 2: Invalid Line Format
# Set 3: 

#print "testing"
