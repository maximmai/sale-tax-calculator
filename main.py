from tokenizer import Tokenizer
import sys
import json

with open('config.json', 'r') as f:
    config = json.load(f)
    

lines = [line.rstrip('\n') for line in open(sys.argv[1])]
total = 0.0
tax = 0.0
for line in lines:
	t = Tokenizer(line, config)
	if t.valid:
		total = total + t.get_total()
		tax = tax + t.tax
		print t
print "Sales Tax:", "{0:.2f}".format(tax)
print "Total:", "{0:.2f}".format(total)